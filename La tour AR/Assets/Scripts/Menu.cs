﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    //Tristan - Deux états du menu
    public GameObject LangSelect;
    public GameObject mainMenu;
    

    // FONCTION StartGame (Boutton Start)

    public void StartGame()
    {
        SceneManager.LoadScene("Game");                                                             // Esteban - Chargement de la scène de jeu.
    }

    //................................................................................................................................

    
        //Tristan - Activer le mode choix de la langue
    public void LanguageGame()
    {
        LangSelect.SetActive(true);
        mainMenu.SetActive(false);
    }

    //Tristan - Revenir au menu normal
    public void BackMenu()
    {
        LangSelect.SetActive(false);
        mainMenu.SetActive(true);
    }

    // FONCTION QuitGame (Boutton Quitter)

    public void QuitGame()
    {
        Application.Quit();                                                                         // Esteban - L'application se ferme.
    }
}
