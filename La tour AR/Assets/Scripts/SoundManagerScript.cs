﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public AudioClip[] sons;

    AudioSource AudioSrc;

    // Start is called before the first frame update
    void Start()
    {
        AudioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InfosDialogue(int _son)
    {
        AudioSrc.Stop();
        AudioSrc.clip = sons[_son];
        AudioSrc.Play();
    }
}
